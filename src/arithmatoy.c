#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

int VERBOSE = 1;

const char *get_all_digits() { return "0123456789abcdefghijklmnopqrstuvwxyz"; }
const size_t ALL_DIGIT_COUNT = 36;

void arithmatoy_free(char *number) { free(number); }

char *arithmatoy_add(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "add: entering function\n");
    fprintf(stderr, "add: lhs = %s\n", lhs);
    fprintf(stderr, "add: rhs = %s\n", rhs);
    fprintf(stderr, "add: base = %i\n", base);
  }
  const char *plhs = drop_leading_zeros(lhs);
  const char *prhs = drop_leading_zeros(rhs);

  char *rplhs = strdup(plhs);
  char *rprhs = strdup(prhs);

  rplhs = reverse(rplhs);
  rprhs = reverse(rprhs);
  const size_t max_len = ALL_DIGIT_COUNT;
  char *result = (char*) malloc(max_len + 1);
  memset(result, 0, max_len);
  result[max_len] = '\0';

  unsigned int carry = 0;
  size_t i = 0;
  while(rplhs[i] != '\0' || rprhs[i] != '\0' || carry != 0)
  {
    unsigned int a = get_digit_value(rplhs[i]);
    unsigned int b = get_digit_value(rprhs[i]);

    if (a == -1) a = 0;
    if (b == -1) b = 0;

    unsigned int sum = a + b + carry;
    carry = sum / base;
    const char digit = to_digit(sum % base);
    result[i] = digit;
    i += 1;
    if(VERBOSE)
    {
      fprintf(stderr, "add: digit %i digit %i carry %i \n", a, b, carry);
    }
  }
  reverse(result);

  free(rplhs);
  free(rprhs);

  char * final_result = strdup(result);
  if(VERBOSE)
  {
    fprintf(stderr, "add: result = %s\n", result);
  }
  arithmatoy_free(result);
  final_result = (char*) drop_leading_zeros(final_result);

  return final_result;
}

char *arithmatoy_sub(unsigned int base, const char *lhs, const char *rhs) {
    if (VERBOSE) {
    fprintf(stderr, "sub: entering function\n");
    fprintf(stderr, "sub: lhs = %s\n", lhs);
    fprintf(stderr, "sub: rhs = %s\n", rhs);
    fprintf(stderr, "sub: base = %i\n", base);
  }
  const char *plhs = drop_leading_zeros(lhs);
  const char *prhs = drop_leading_zeros(rhs);

  char *rplhs = strdup(plhs);
  char *rprhs = strdup(prhs);

  rplhs = reverse(rplhs);
  rprhs = reverse(rprhs);
  const size_t max_len = ALL_DIGIT_COUNT;
  char *result = (char*) malloc(max_len + 1);
  memset(result, 0, max_len);
  result[max_len] = '\0';

  unsigned int carry = 0;
  size_t i = 0;
  while(rplhs[i] != '\0' || rprhs[i] != '\0' || carry != 0)
  {
    unsigned int a = get_digit_value(rplhs[i]);
    unsigned int b = get_digit_value(rprhs[i]);
    unsigned int sum;

    if (a == -1) a = 0;
    if (b == -1) b = 0;
    if (a == 0 && b == 0 && carry == 1) return NULL;

    if (a >= b + carry){
      sum = a - b - carry;
      carry = 0;
    }
    else if(a < b + carry){
      sum = base + a - b - carry;
      carry = 1;
    }
    const char digit = to_digit(sum);
    result[i] = digit;

    i += 1;

    if(VERBOSE)
    {
      fprintf(stderr, "sub: digit %i digit %i carry %i \n", a, b, carry);
    }
  }
  reverse(result);

  free(rplhs);
  free(rprhs);

  char * final_result = strdup(result);
  if(VERBOSE)
  {
    fprintf(stderr, "sub: result = %s\n", result);
  }
  arithmatoy_free(result);
  final_result = (char*) drop_leading_zeros(final_result);

  return final_result;
}

char *arithmatoy_mul(unsigned int base, const char *lhs, const char *rhs) {
    if (VERBOSE) {
    fprintf(stderr, "mul: entering function\n");
    fprintf(stderr, "mul: lhs = %s\n", lhs);
    fprintf(stderr, "mul: rhs = %s\n", rhs);
    fprintf(stderr, "mul: base = %i\n", base);
  }
  const char *plhs = drop_leading_zeros(lhs);
  const char *prhs = drop_leading_zeros(rhs);

  char *rplhs = reverse(strdup(plhs));
  char *rprhs = reverse(strdup(prhs));

  const size_t max_len = ALL_DIGIT_COUNT;
  char *result = (char*) malloc(max_len + 1);

  for (size_t i = 0; rprhs[i] != '\0'; ++i)
  {
    unsigned int carry = 0;
    for(unsigned int j = 0; rplhs[j] != '\0'; j += 1){
      unsigned int a = get_digit_value(rplhs[j]);
      unsigned int b = get_digit_value(rprhs[i]);
      if (b == -1) b = 0;
      if (a == -1) a = 0;

      unsigned int sum = a * b + carry;
      unsigned int before = get_digit_value(result[i + j]);
      if (before == -1) before = 0;
      carry = (before + sum) / base;

      if(VERBOSE) fprintf(stderr, "mul: left_digit = %i right_digit =  %i  result = %i carry = %i before = %i \n", a, b, sum % base, carry, before);

      const char digit = to_digit((before + sum) % base);
      result[i + j] = digit;
    }

     if (carry != 0) {
      result[i + strlen(rplhs)] += carry;
    }


  }
  reverse(result);
  result = drop_leading_zeros(result);

  free(rplhs);
  free(rprhs);

  if(VERBOSE)
  {
    fprintf(stderr, "mul: result = %s\n", result);
  }

  return result;
}



unsigned int get_digit_value(char digit) {
  // Convert a digit from get_all_digits() to its integer value
  if (digit >= '0' && digit <= '9') {
    return digit - '0';
  }
  if (digit >= 'a' && digit <= 'z') {
    return 10 + (digit - 'a');
  }
  return -1;
}

char to_digit(unsigned int value) {
  // Convert an integer value to a digit from get_all_digits()
  if (value >= ALL_DIGIT_COUNT) {
    debug_abort("Invalid value for to_digit()");
    return 0;
  }
  return get_all_digits()[value];
}

char *reverse(char *str) {
  // Reverse a string in place, return the pointer for convenience
  // Might be helpful if you fill your char* buffer from left to right
  const size_t length = strlen(str);
  const size_t bound = length / 2;
  for (size_t i = 0; i < bound; ++i) {
    char tmp = str[i];
    const size_t mirror = length - i - 1;
    str[i] = str[mirror];
    str[mirror] = tmp;
  }
  return str;
}

const char *drop_leading_zeros(const char *number) {
  // If the number has leading zeros, return a pointer past these zeros
  // Might be helpful to avoid computing a result with leading zeros
  if (*number == '\0') {
    return number;
  }
  while (*number == '0') {
    ++number;
  }
  if (*number == '\0') {
    --number;
  }
  return number;
}

void debug_abort(const char *debug_msg) {
  // Print a message and exit
  fprintf(stderr, debug_msg);
  exit(EXIT_FAILURE);
}
